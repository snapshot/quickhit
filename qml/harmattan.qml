/**
 * Copyright (c) 2011 Nokia Corporation.
 */

import QtQuick 1.1
import com.nokia.meego 1.0

Window {
    Page {
        // Lock the screen orientation to portrait.
        orientationLock: PageOrientation.LockPortrait
    }
}
